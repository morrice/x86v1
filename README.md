# x86v1

* wip to build x86_64-v1 versions of packages from alma9
* needs [custom redhat-rpm-config rpm](https://gitlab.cern.ch/morrice/redhat-rpm-config)

## details

* after all rpms are built with march=x86_64 (`build.sh`), run `createrepo` with a comps.xml as the anaconda installer will expect that the 'Core' group is available

## testing

Initial VM bootstrap (pxeboot kernel/initrd are from upstream and thus are compiled with x86_64-v2):
`virt-install --ram 4096 --vcpus 4 --location http://linuxsoft.cern.ch/tmp/x86v1/BaseOS/x86_64/os`

Once VM has been installed, boot the disk image with `--cpu Penryn` to run on a virtual CPU that doesn't have x86_64-v2 instructions (Penryn is from around 2008). We do the undefine as it's easier than editing the virsh xml manually

* `virsh undefine <name>`
* `virt-install --ram 4096 --vcpus 4 --os-variant almalinux9 --disk /path/to/qcow/probably/in/home/.local/share/libvirt/images/something.qcow2 --import --cpu Penryn`
