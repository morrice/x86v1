#!/bin/bash
if [ $# -eq 0 ]; then
  PACKAGES=$(dnf --repofrompath=tmpbaseos,https://linuxsoft.cern.ch/cern/alma/9/BaseOS/x86_64/os/ --repo=tmpbaseos list --available |grep tmpbaseos | awk '{print $1}' | cut -d. -f1)
else
  PACKAGES=$1
fi
RESULT="/var/lib/mock/almalinux-9-x86_64/result"
INPUT="/root/input"
OUTPUT="/root/output"
for PACKAGE in $PACKAGES
do
  if [ $(ls ${OUTPUT}/${PACKAGE}* | wc -l ) -gt 0 ]; then
    continue
  fi
  rm -rf ${INPUT}/*src.rpm
  dnf download --source ${PACKAGE} --destdir=${INPUT}
  mock --nocheck -r x86v1 $(ls ${INPUT}/*src.rpm)
  rm -f ${RESULT}/{*debuginfo*,*debugsource*,*src.rpm}
  mv ${RESULT}/*.rpm ${OUTPUT}/
done
cd ${OUTPUT}
createrepo .
